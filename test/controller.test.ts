import { PrismaClient } from "@prisma/client";
import * as controller from "../src/controller";

const prisma = new PrismaClient();

describe("add city", () => {
  beforeAll(async () => {
    await prisma.city.create({
      data: {
        name: "Chemnitz",
        partner: {
          create: {
            name: "Dresden",
            distance: 20,
          },
        },
      },
    });
  });

  afterAll(async () => {
    const deleteCityOrders = prisma.city.deleteMany();
    const deletePartnerOrders = prisma.partner.deleteMany();

    await prisma.$transaction([deleteCityOrders, deletePartnerOrders]);
    await prisma.$disconnect();
  });

  it("should create a city entry", async () => {
    const partner = {
      Berlin: 10,
    };

    const name = "Leipzig";

    const city = await controller.addCity(name, partner);

    expect(city).toEqual({ id: expect.any(Number), name, partner });
  });

  it("should get a single city entry", async () => {
    const city = await prisma.city.create({
      data: {
        name: "Nuernberg",
        partner: {
          create: {
            name: "Leipzig",
            distance: 20,
          },
        },
      },
    });

    const result = await controller.getCity(city.id);

    expect(result).toEqual({
      id: expect.any(Number),
      name: "Nuernberg",
      partner: { Leipzig: 20 },
    });
  });

  it("should get a list of cities", async () => {
    const result = await controller.getCities();

    expect(result).toHaveLength(3);
    expect(result).toContainEqual({
      id: expect.any(Number),
      name: "Nuernberg",
      partner: { Leipzig: 20 },
    });
  });

  it("should delete a city", async () => {
    const city = await prisma.city.create({
      data: {
        name: "Frankfurt",
        partner: {
          create: {
            name: "Nurenberg",
            distance: 30,
          },
        },
      },
    });

    await controller.deleteCity(city.id);

    expect(await controller.getCity(city.id)).toBeNull();
  });
});
