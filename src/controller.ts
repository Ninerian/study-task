import { City, Partner, PrismaClient } from "@prisma/client";

// create city / cities
// get city / cities
// add unknown partner -> new city
//
// delete partner
// delete city -> delete partner in cities
//
//
//
const prisma = new PrismaClient();

export const addCity = async (
  name: string,
  partner: Record<string, number>
) => {
  const city = await prisma.city.create({
    data: {
      name: name,
    },
  });

  await Object.entries(partner).forEach(async ([pName, distance]) => {
    await prisma.partner.create({
      data: {
        name: pName,
        distance: distance,
        cityId: city.id,
      },
    });
  });

  return getCity(city.id);
};

type CityObject = City & { partner: Partner[] };

const cityObjectMapper = (cityObject: CityObject | null) => {
  const partners = cityObject?.partner.reduce((acc, p) => {
    acc[p.name] = p.distance;
    return acc;
  }, {} as Record<string, number>);

  return cityObject
    ? {
        id: cityObject.id,
        name: cityObject.name,
        partner: partners,
      }
    : null;
};

export const getCity = async (id: number) => {
  const cityObject = await prisma.city.findUnique({
    where: {
      id: id,
    },
    include: {
      partner: true,
    },
  });

  return cityObjectMapper(cityObject);
};

export const getCities = async () => {
  const cities = await prisma.city.findMany({
    include: {
      partner: true,
    },
  });

  return cities?.map(cityObjectMapper).filter((c) => !!c);
};

export const deleteCity = async (id: number) => {
  await prisma.city.delete({ where: { id } });
};
